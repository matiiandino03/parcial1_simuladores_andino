using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Monopoly
{
    public class Cartas_Suerte : Carta
    {
    }
    public class Carta1Suerte : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Fuiste perdonado por el presidente, con esta tarjeta sales de prision";
            jugador.perdonadoCarcel = true;
            SetearCartel();
        }
    }
    public class Carta2Suerte : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Avanzas hasta la salida y cobras $200";
            jugador.posicion = 0;
            jugador.dinero += 200;
            GameManager.instance.MoverJugadoresEnTablero();
            SetearCartel();
        }
    }
    public class Carta3Suerte : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Impuesto por el servicio de gas, paga $15 al banco";
            if(jugador.dinero >= 15)
            {
                jugador.dinero -= 15;
                SetearCartel();
            }
            if(jugador.dinero <= 15)
            {
                NoTieneDinero(jugador);
            }
        }
    }
    public class Carta4Suerte : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            int hoteles = 0;
            int casas = 0;
            queHace = "Arreglos en tus propiedades, paga $25 por cada casa y $100 por cada hotel.";
            foreach(Propiedad prop in jugador.misCasilleros)
            {
                casas += prop.cantidadCasas;
                if(prop.hotel)
                {
                    hoteles++;
                }
            }
            if(jugador.dinero >= ((casas * 25) + (hoteles * 100)))
            {
                jugador.dinero -= ((casas * 25) + (hoteles * 100));
                SetearCartel();
            }
            if(jugador.dinero <= ((casas * 25) + (hoteles * 100)))
            {
                NoTieneDinero(jugador);
            }
        }
    }
    public class Carta5Suerte : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Error de calculos, el banco te devuelve $50";
            jugador.dinero += 50;
            SetearCartel();
        }
    }
    public class Carta6Suerte : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Tus construcciones estan mejorando, toma $150 del banco";
            jugador.dinero += 150;
            SetearCartel();
        }
    }
    public class Carta7Suerte : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Arrestado por lavado de dinero, ve a la carcel y si pasas por la salida no cobres nada";
            jugador.penalizacionTurnos = 3;
            jugador.posicion = 10;
            GameManager.instance.MoverJugadoresEnTablero();
            SetearCartel();
        }
    }
    public class Carta8Suerte : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador,Jugador jugador)
        {
            queHace = "Has recibido un llamado, avanza hasta mendoza sur";
            jugador.posicion = 23;
            GameManager.instance.MoverJugadoresEnTablero();
            SetearCartel();
        }
    }
    public class Carta9Suerte : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Deudas entre amigos, pagas $25 a cada jugador";
            if(jugador.dinero >= ((GameManager.instance.jugadores.Count - 1) * 25))
            {
                jugador.dinero -= ((GameManager.instance.jugadores.Count - 1) * 25);
                foreach (Jugador pj in GameManager.instance.jugadores)
                {
                    if (pj != jugador)
                    {
                        pj.dinero += 25;
                    }
                }
                SetearCartel();
            }
            if(jugador.dinero <= ((GameManager.instance.jugadores.Count - 1) * 25))
            {
                NoTieneDinero(jugador);
            }
        }
    }
}

