using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Monopoly
{
    public class Tablero
    {
        public List<Casillero> Casilleros = new List<Casillero>();
        public List<Carta> CartasSuerte = new List<Carta>();
        public List<Carta> CartasArcaComunal = new List<Carta>();

        public void CrearCasilleros()
        {
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.salida));
            Casilleros.Add(new Propiedad("SAN LUIS NORTE",Casillero.type.propiedad, 60, 30, Propiedad.Color.bordo));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.cofre));
            Casilleros.Add(new Propiedad("SAN LUIS SUR", Casillero.type.propiedad, 60, 30, Propiedad.Color.bordo));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.impuesto,200));
            Casilleros.Add(new Propiedad("TREN NORTE", Casillero.type.estacionTren,200,100,true));
            Casilleros.Add(new Propiedad("FORMOSA ESTE", Casillero.type.propiedad, 100, 50, Propiedad.Color.celeste));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.suerte));
            Casilleros.Add(new Propiedad("FORMOSA NORTE", Casillero.type.propiedad, 100, 50, Propiedad.Color.celeste));
            Casilleros.Add(new Propiedad("FORMOSA SUR", Casillero.type.propiedad, 120, 60, Propiedad.Color.celeste));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.carcel));
            Casilleros.Add(new Propiedad("SAN JUAN ESTE", Casillero.type.propiedad, 140, 70, Propiedad.Color.rosa));
            Casilleros.Add(new Propiedad(true,"COMPANIA LUZ", Casillero.type.servicioLuz, 150, 75));
            Casilleros.Add(new Propiedad("SAN JUAN SUR", Casillero.type.propiedad, 140, 70, Propiedad.Color.rosa));
            Casilleros.Add(new Propiedad("SAN JUAN NORTE", Casillero.type.propiedad, 160, 80, Propiedad.Color.rosa));
            Casilleros.Add(new Propiedad("TREN OESTE", Casillero.type.estacionTren, 200, 100, true));
            Casilleros.Add(new Propiedad("NEUQUEN ESTE", Casillero.type.propiedad, 180, 90, Propiedad.Color.naranja));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.cofre));
            Casilleros.Add(new Propiedad("NEUQUEN SUR", Casillero.type.propiedad, 180, 90, Propiedad.Color.naranja));
            Casilleros.Add(new Propiedad("NEUQUEN NORTE", Casillero.type.propiedad, 200, 100, Propiedad.Color.naranja));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.free));
            Casilleros.Add(new Propiedad("MENDOZA ESTE", Casillero.type.propiedad, 220, 110, Propiedad.Color.rojo));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.suerte));
            Casilleros.Add(new Propiedad("MENDOZA SUR", Casillero.type.propiedad, 220, 110, Propiedad.Color.rojo));
            Casilleros.Add(new Propiedad("MENDOZA NORTE", Casillero.type.propiedad, 240, 120, Propiedad.Color.rojo));
            Casilleros.Add(new Propiedad("TREN ESTE", Casillero.type.estacionTren, 200, 100, true));
            Casilleros.Add(new Propiedad("SANTA FE ESTE", Casillero.type.propiedad, 260, 130, Propiedad.Color.amarillo));
            Casilleros.Add(new Propiedad("SANTA FE SUR", Casillero.type.propiedad, 260, 130, Propiedad.Color.amarillo));
            Casilleros.Add(new Propiedad(true, "COMPANIA AGUA", Casillero.type.servicioAgua, 150, 75));
            Casilleros.Add(new Propiedad("SANTA FE NORTE", Casillero.type.propiedad, 280, 140, Propiedad.Color.amarillo));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.goToJail));
            Casilleros.Add(new Propiedad("CORDOBA ESTE", Casillero.type.propiedad, 300, 150, Propiedad.Color.verde));
            Casilleros.Add(new Propiedad("CORDOBA SUR", Casillero.type.propiedad, 300, 150, Propiedad.Color.verde));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.cofre));
            Casilleros.Add(new Propiedad("CORDOBA NORTE", Casillero.type.propiedad, 320, 160, Propiedad.Color.verde));
            Casilleros.Add(new Propiedad("TREN SUR", Casillero.type.estacionTren, 200, 100, true));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.suerte));
            Casilleros.Add(new Propiedad("BUENOS AIRES NORTE", Casillero.type.propiedad, 350, 175, Propiedad.Color.azul));
            Casilleros.Add(new Casilleros_Especiales(Casillero.type.impuesto,75));
            Casilleros.Add(new Propiedad("BUENOS AIRES SUR", Casillero.type.propiedad, 400, 200, Propiedad.Color.azul));
        }

        public void LlenarCartasSuerte()
        {
            CartasSuerte.Add(new Carta1Suerte());
            CartasSuerte.Add(new Carta2Suerte());
            CartasSuerte.Add(new Carta3Suerte());
            CartasSuerte.Add(new Carta4Suerte());
            CartasSuerte.Add(new Carta5Suerte());
            CartasSuerte.Add(new Carta6Suerte());
            CartasSuerte.Add(new Carta7Suerte());
            CartasSuerte.Add(new Carta8Suerte());
            CartasSuerte.Add(new Carta9Suerte());
        }
        public void LlenarCartasArcaComunal()
        {
            CartasArcaComunal.Add(new Carta1Comunal());
            CartasArcaComunal.Add(new Carta2Comunal());
            CartasArcaComunal.Add(new Carta3Comunal());
            CartasArcaComunal.Add(new Carta4Comunal());
            CartasArcaComunal.Add(new Carta5Comunal());
            CartasArcaComunal.Add(new Carta6Comunal());
            CartasArcaComunal.Add(new Carta7Comunal());
            CartasArcaComunal.Add(new Carta8Comunal());
            CartasArcaComunal.Add(new Carta9Comunal());
            CartasArcaComunal.Add(new Carta10Comunal());
            CartasArcaComunal.Add(new Carta11Comunal());
            CartasArcaComunal.Add(new Carta12Comunal());
            CartasArcaComunal.Add(new Carta13Comunal());
        }
    }
}
