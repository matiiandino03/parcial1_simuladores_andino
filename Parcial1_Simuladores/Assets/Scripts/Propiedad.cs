using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace Monopoly
{
    public class Propiedad : Casillero
    {
        public enum owner { nulo, jugador1, jugador2, jugador3, jugador4 }
        public owner ownerEnum;

        public int cantidadCasas;

        public bool hotel; 

        public enum Color { amarillo , rojo , verde , azul , celeste , naranja , rosa , bordo}

        public Color color;

        public bool esEstacion = false;
        public bool esServicio = false;

        public int rentaBase;
        public Propiedad(string mNombre, Casillero.type tipo , int mCosto , int mRenta , Color pColor)
        {
            nombre = mNombre;
            typeEnum = tipo;
            costo = mCosto;
            renta = mRenta;
            comprable = true;
            color = pColor;
            rentaBase = mRenta;
        }
        public Propiedad(string mNombre, Casillero.type tipo, int mCosto, int mRenta, bool estacionTren)
        {
            nombre = mNombre;
            typeEnum = tipo;
            costo = mCosto;
            renta = mRenta;
            comprable = true;
            esEstacion = estacionTren;
            rentaBase = mRenta;
        }
        public Propiedad(bool mesServicio ,string mNombre, Casillero.type tipo, int mCosto, int mRenta)
        {
            nombre = mNombre;
            typeEnum = tipo;
            costo = mCosto;
            renta = mRenta;
            comprable = true;
            esServicio = mesServicio;
            rentaBase = mRenta;
        }

        public void ComprarPropiedad(owner comprador , Jugador jugador)
        {
            if(jugador.dinero >= costo)
            {
                ownerEnum = comprador;
                jugador.dinero -= costo;
                jugador.misCasilleros.Add(this);

                if (color == Color.amarillo)
                {
                    int controlCantidad = 0;
                    foreach(Propiedad prop in jugador.misCasilleros)
                    {
                        if(prop.color == Color.amarillo)
                        {
                            controlCantidad++;
                        }
                    }
                    if(controlCantidad == 3)
                    {
                        jugador.tengoPropsAmarillas = true;
                        foreach (Propiedad prop in jugador.misCasilleros)
                        {
                            if (color == Color.amarillo)
                            {
                                prop.renta = prop.renta*2;
                            }
                        }
                    }
                }
                if (color == Color.rojo)
                {
                    int controlCantidad = 0;
                    foreach (Propiedad prop in jugador.misCasilleros)
                    {
                        if (prop.color == Color.rojo)
                        {
                            controlCantidad++;
                        }
                    }
                    if (controlCantidad == 3)
                    {
                        foreach (Propiedad prop in jugador.misCasilleros)
                        {
                            jugador.tengoPropsRojas = true;
                            if (color == Color.rojo)
                            {
                                prop.renta = prop.renta * 2;
                            }
                        }
                    }
                }
                if (color == Color.rosa)
                {
                    int controlCantidad = 0;
                    foreach (Propiedad prop in jugador.misCasilleros)
                    {
                        if (prop.color == Color.rosa)
                        {
                            controlCantidad++;
                        }
                    }
                    if (controlCantidad == 3)
                    {
                        jugador.tengoPropsRosas = true;
                        foreach (Propiedad prop in jugador.misCasilleros)
                        {
                            if (color == Color.rosa)
                            {
                                prop.renta = prop.renta * 2;
                            }
                        }
                    }
                }
                if (color == Color.verde)
                    {
                        int controlCantidad = 0;
                        foreach (Propiedad prop in jugador.misCasilleros)
                        {
                            if (prop.color == Color.verde)
                            {
                                controlCantidad++;
                            }
                        }
                        if (controlCantidad == 3)
                        {
                        jugador.tengoPropsVerdes = true;
                            foreach (Propiedad prop in jugador.misCasilleros)
                            {
                                if (color == Color.verde)
                                {
                                    prop.renta = prop.renta * 2;
                                }
                            }
                        }
                    }
                if (color == Color.azul)
                {
                    int controlCantidad = 0;
                    foreach (Propiedad prop in jugador.misCasilleros)
                    {
                        if (prop.color == Color.azul)
                        {
                            controlCantidad++;
                        }
                    }
                    if (controlCantidad == 2)
                    {
                        jugador.tengoPropsAzules = true;
                        foreach (Propiedad prop in jugador.misCasilleros)
                        {
                            if (color == Color.azul)
                            {
                                prop.renta = prop.renta * 2;
                            }
                        }
                    }
                }
                if (color == Color.bordo)
                {
                    int controlCantidad = 0;
                    foreach (Propiedad prop in jugador.misCasilleros)
                    {
                        if (prop.color == Color.bordo)
                        {
                            controlCantidad++;
                        }
                    }
                    if (controlCantidad == 2)
                    {
                        jugador.tengoPropsBordo = true;
                        foreach (Propiedad prop in jugador.misCasilleros)
                        {
                            if (color == Color.bordo)
                            {
                                prop.renta = prop.renta * 2;
                            }
                        }
                    }
                }
                if (color == Color.celeste)
                {
                    int controlCantidad = 0;
                    foreach (Propiedad prop in jugador.misCasilleros)
                    {
                        if (prop.color == Color.celeste)
                        {
                            controlCantidad++;
                        }
                    }
                    if (controlCantidad == 3)
                    {
                        jugador.tengoPropsCelestes = true;
                        foreach (Propiedad prop in jugador.misCasilleros)
                        {
                            if (color == Color.celeste)
                            {
                                prop.renta = prop.renta * 2;
                            }
                        }
                    }
                }
                if (color == Color.naranja)
                {
                    int controlCantidad = 0;
                    foreach (Propiedad prop in jugador.misCasilleros)
                    {
                        if (prop.color == Color.naranja)
                        {
                            controlCantidad++;
                        }
                    }
                    if (controlCantidad == 3)
                    {
                        jugador.tengoPropsNaranjas = true;
                        foreach (Propiedad prop in jugador.misCasilleros)
                        {
                            if (color == Color.naranja)
                            {
                                prop.renta = prop.renta * 2;
                            }
                        }
                    }
                }

                GameManager.instance.EndTurn();
            }
            else
            {
                GameManager.instance.txtAcciones.text = "No puedes comprar esta propiedad por falta de dinero";
            }
        }

        public void ContruirCasa(owner comprador, Jugador jugador)
        {
            if(cantidadCasas < 4 && jugador.dinero >= 50 && renta > rentaBase)
            {
                cantidadCasas++;
                renta += 40;
                jugador.dinero -= 50;
            }
            else
            {
                GameManager.instance.txtAcciones.text = "No puedes construir una casa porque no tienes el dinero suficiente o no posees todas las propiedades de este color";                
                JugadorOnCasillero(GameManager.instance.jugadores[GameManager.instance.turno]);
            }
        }

        public void ContruirHotel(owner comprador, Jugador jugador)
        {
            if(cantidadCasas == 4 && jugador.dinero >= 50)
            {
                hotel = true;
                cantidadCasas = 0;
                renta += 30;
                jugador.dinero -= 50;
                GameManager.instance.EndTurn();
            }
            else
            {
                GameManager.instance.txtAcciones.text = "No puedes construir un hotel, necesitas tener 4 casas";
                JugadorOnCasillero(GameManager.instance.jugadores[GameManager.instance.turno]);
            }
        }

        public void PagarRenta(owner comprador, Jugador jugador)
        {
            if(jugador.dinero >= renta)
            {
                jugador.dinero -= renta;
                if(ownerEnum == owner.jugador1)
                {
                    GameManager.instance.jugadores[0].dinero += renta;
                }
                if (ownerEnum == owner.jugador2)
                {
                    GameManager.instance.jugadores[1].dinero += renta;
                }
                if (ownerEnum == owner.jugador3)
                {
                    GameManager.instance.jugadores[2].dinero += renta;
                }
                if (ownerEnum == owner.jugador4)
                {
                    GameManager.instance.jugadores[3].dinero += renta;
                }
                GameManager.instance.EndTurn();
            }
            else
            {
                GameManager.instance.txtAcciones.text = "Necesitas $" + renta.ToString() + ", vende tus propiedades para pagar. Si no te alcanza estas en banca rota";
            }
        }

        public override void JugadorOnCasillero(Jugador jugador)
        {
            if(ownerEnum == jugador.enumOwnerJugador)
            {
                GameManager.instance.delegateBt1 = ContruirCasa;
                GameManager.instance.delegateBt2 = ContruirHotel;
                GameManager.instance.delegateBt3 = GameManager.instance.EndTurn;
                GameManager.instance.txtAcciones.text = "Eres el due�o de esta propiedad, puedes construir estructuras en la misma o terminar tu turno";
                GameManager.instance.bt2.SetActive(true);
                GameManager.instance.bt1.SetActive(true);
                GameManager.instance.bt3.SetActive(true);
                GameManager.instance.bt1.GetComponentInChildren<Text>().text = "Construir casa";
                GameManager.instance.bt2.GetComponentInChildren<Text>().text = "Construir hotel";
                GameManager.instance.bt3.GetComponentInChildren<Text>().text = "Terminar turno";
                GameManager.instance.CartelAcciones.SetActive(true);
            }
            if(ownerEnum == owner.nulo)
            {
                GameManager.instance.delegateBt1 = ComprarPropiedad;
                GameManager.instance.delegateBt3 = GameManager.instance.EndTurn;
                GameManager.instance.txtAcciones.text = "�Quieres comprar esta propiedad por un costo de $" + costo.ToString() + "?";
                GameManager.instance.bt2.SetActive(false);
                GameManager.instance.bt1.SetActive(true);
                GameManager.instance.bt3.SetActive(true);
                GameManager.instance.bt1.GetComponentInChildren<Text>().text = "Comprar";
                GameManager.instance.bt3.GetComponentInChildren<Text>().text = "Terminar turno";
                GameManager.instance.CartelAcciones.SetActive(true);
            }
            else
            {
                GameManager.instance.bt1.SetActive(false);
                GameManager.instance.bt3.SetActive(false);
                GameManager.instance.bt2.SetActive(true);
                GameManager.instance.delegateBt2 = PagarRenta;
                GameManager.instance.txtAcciones.text = "Debes pagar " + renta.ToString() + " al jugador " + ownerEnum.ToString();
                GameManager.instance.bt2.GetComponentInChildren<Text>().text = "Pagar Renta";
                GameManager.instance.CartelAcciones.SetActive(true);
            }
        }

        public void ChequearSiTieneDelMismoColor()
        {

        }
    }
}

