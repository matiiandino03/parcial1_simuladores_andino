using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Monopoly;

public class MyCasilleros : MonoBehaviour
{
    public Text txtNombre;
    public Text txtEstructuras;
    public Text txtRenta;
    public Text txtVenta;
    string nombreProp;
    public void SetearTxt(Propiedad prop)
    {
        txtNombre.text = prop.nombre;
        if(prop.hotel)
        {
            txtEstructuras.text = "Hotel: 1";
        }
        else
        {
            txtEstructuras.text = "Casas: " + prop.cantidadCasas.ToString();
        }
        txtRenta.text = "Renta: " + prop.renta.ToString();
        txtVenta.text = "Precio venta: " + (prop.costo/2).ToString();
        nombreProp = prop.nombre;
    }

    public void VenderPropiedad()
    {
        for (int i = 0; i < GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros.Count; i++)
        {
            if (GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].nombre == nombreProp)
            {
                if (GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].color == Propiedad.Color.amarillo && GameManager.instance.jugadores[GameManager.instance.turno].tengoPropsAmarillas)
                {
                    foreach (Propiedad prop in GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros)
                    {
                        if (prop.color == Propiedad.Color.amarillo)
                        {
                            prop.renta = prop.renta / 2;
                        }
                    }
                }
                if (GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].color == Propiedad.Color.rojo && GameManager.instance.jugadores[GameManager.instance.turno].tengoPropsRojas)
                {
                    foreach (Propiedad prop in GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros)
                    {
                        if (prop.color == Propiedad.Color.rojo)
                        {
                            prop.renta = prop.renta / 2;
                        }
                    }
                }
                if (GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].color == Propiedad.Color.verde && GameManager.instance.jugadores[GameManager.instance.turno].tengoPropsVerdes)
                {
                    foreach (Propiedad prop in GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros)
                    {
                        if (prop.color == Propiedad.Color.verde)
                        {
                            prop.renta = prop.renta / 2;
                        }
                    }
                }
                if (GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].color == Propiedad.Color.rosa && GameManager.instance.jugadores[GameManager.instance.turno].tengoPropsRosas)
                {
                    foreach (Propiedad prop in GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros)
                    {
                        if (prop.color == Propiedad.Color.rosa)
                        {
                            prop.renta = prop.renta / 2;
                        }
                    }
                }
                if (GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].color == Propiedad.Color.bordo && GameManager.instance.jugadores[GameManager.instance.turno].tengoPropsBordo)
                {
                    foreach (Propiedad prop in GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros)
                    {
                        if (prop.color == Propiedad.Color.bordo)
                        {
                            prop.renta = prop.renta / 2;
                        }
                    }
                }
                if (GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].color == Propiedad.Color.naranja && GameManager.instance.jugadores[GameManager.instance.turno].tengoPropsNaranjas)
                {
                    foreach (Propiedad prop in GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros)
                    {
                        if (prop.color == Propiedad.Color.naranja)
                        {
                            prop.renta = prop.renta / 2;
                        }
                    }
                }
                if (GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].color == Propiedad.Color.azul && GameManager.instance.jugadores[GameManager.instance.turno].tengoPropsAzules)
                {
                    foreach (Propiedad prop in GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros)
                    {
                        if (prop.color == Propiedad.Color.azul)
                        {
                            prop.renta = prop.renta / 2;
                        }
                    }
                }
                if (GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].color == Propiedad.Color.celeste && GameManager.instance.jugadores[GameManager.instance.turno].tengoPropsCelestes)
                {
                    foreach (Propiedad prop in GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros)
                    {
                        if (prop.color == Propiedad.Color.celeste)
                        {
                            prop.renta = prop.renta / 2;
                        }
                    }
                }

                GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].ownerEnum = Propiedad.owner.nulo;
                GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].cantidadCasas = 0;
                GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].hotel = false;
                GameManager.instance.jugadores[GameManager.instance.turno].dinero += Mathf.RoundToInt(GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i].costo / 2);
                GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros.Remove(GameManager.instance.jugadores[GameManager.instance.turno].misCasilleros[i]);
            }
        }
        GameManager.instance.DestruirPropsAdq();
        GameManager.instance.LlenarCasillerosAdquiridos();
    }
}
