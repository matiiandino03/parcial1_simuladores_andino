using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Monopoly
{
    public class Casillero
    {
        public enum type { propiedad , salida , carcel , goToJail , free , estacionTren , servicioLuz , servicioAgua , suerte , cofre , impuesto}
        public type typeEnum;

        public int costo;

        public int renta;

        public bool agarrarCarta;

        public string nombre;

        public bool comprable;

        public virtual void JugadorOnCasillero(Jugador jugador) { }

    }
}
