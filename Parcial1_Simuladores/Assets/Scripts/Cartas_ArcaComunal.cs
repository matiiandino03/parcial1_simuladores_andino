using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Monopoly
{
    public class Cartas_ArcaComunal : Carta
    {
    }
    public class Carta1Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Han pagado la fianza, con esta tarjeta sales de prision";
            jugador.perdonadoCarcel = true;
            SetearCartel();
        }
    }
    public class Carta2Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Has donado dinero para la educacion de los jovenes, paga $150 al banco";
            if (jugador.dinero >= 150)
            {
                jugador.dinero -= 150;
                SetearCartel();
            }
            if (jugador.dinero <= 150)
            {
                NoTieneDinero(jugador);
            }
        }
    }
    public class Carta3Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Error del banco a tu favor, recibes $200";
            jugador.dinero += 200;
            SetearCartel();
        }
    }
    public class Carta4Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Tu abuela murio, heredas $100";
            jugador.dinero += 100;
            SetearCartel();
        }
    }
    public class Carta5Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Compraste juguetes para los ni�os pobres en navidad, paga $100 al banco";
            if (jugador.dinero >= 100)
            {
                jugador.dinero -= 100;
                SetearCartel();
            }
            if (jugador.dinero <= 100)
            {
                NoTieneDinero(jugador);
            }
        }
    }
    public class Carta6Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Servicios de cirugia plastica, paga $100 al banco";
            if (jugador.dinero >= 100)
            {
                jugador.dinero -= 100;
                SetearCartel();
            }
            if (jugador.dinero <= 100)
            {
                NoTieneDinero(jugador);
            }
        }
    }
    public class Carta7Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {           
            queHace = "Primer lugar en concurso de belleza, cobras $50";
            jugador.dinero += 50;
            SetearCartel();
        }
    }
    public class Carta8Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Avanzas hasta la salida y cobras $200";
            jugador.posicion = 0;
            jugador.dinero += 200;
            GameManager.instance.MoverJugadoresEnTablero();
            SetearCartel();
        }
    }
    public class Carta9Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            int hoteles = 0;
            int casas = 0;
            queHace = "Has sido multado por construcciones inseguras, paga $25 por cada casa y $100 por cada hotel.";
            foreach (Propiedad prop in jugador.misCasilleros)
            {
                casas += prop.cantidadCasas;
                if (prop.hotel)
                {
                    hoteles++;
                }
            }
            if(jugador.dinero >= ((casas * 25) + (hoteles * 100)))
            {
                jugador.dinero -= ((casas * 25) + (hoteles * 100));
                SetearCartel();
            }
            if (jugador.dinero <= ((casas * 25) + (hoteles * 100)))
            {
                NoTieneDinero(jugador);
            }
        }
    }
    public class Carta10Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Gastos en electrodomesticos, pagas $30";
            if (jugador.dinero >= 30)
            {
                jugador.dinero -= 30;
                SetearCartel();
            }
            if (jugador.dinero <= 30)
            {
                NoTieneDinero(jugador);
            }
        }
    }
    public class Carta11Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Multa de transito, pagas $50";
            if(jugador.dinero >= 50)
            {
                jugador.dinero -= 50;
                SetearCartel();
            }
            if(jugador.dinero <= 50)
            {
                NoTieneDinero(jugador);
            }
        }
    }
    public class Carta12Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Te encontraron robandole a tus compa�eros, pagas $50 a cada uno";
            if(jugador.dinero >= ((GameManager.instance.jugadores.Count - 1) * 50))
            {
                jugador.dinero -= ((GameManager.instance.jugadores.Count - 1) * 50);
                foreach (Jugador pj in GameManager.instance.jugadores)
                {
                    if (pj != jugador)
                    {
                        pj.dinero += 50;
                    }
                }
                SetearCartel();
            }
            if(jugador.dinero <= ((GameManager.instance.jugadores.Count - 1) * 50))
            {
                NoTieneDinero(jugador);
            }
        }
    }
    public class Carta13Comunal : Carta
    {
        public override void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador)
        {
            queHace = "Arrestado por evadir impuestos, ve a la carcel y si pasas por la salida no cobres nada";
            jugador.penalizacionTurnos = 3;
            jugador.posicion = 10;
            GameManager.instance.MoverJugadoresEnTablero();
            SetearCartel();
        }
    }
}

