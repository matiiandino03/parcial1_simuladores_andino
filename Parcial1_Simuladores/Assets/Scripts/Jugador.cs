using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Monopoly
{
    public class Jugador
    {
        public Propiedad.owner enumOwnerJugador;
        public string nombre;
        public int dinero;
        public int posicion = 0;
        public bool bancaRota = false;
        public int penalizacionTurnos = 0;
        public List<Propiedad> misCasilleros = new List<Propiedad>();
        public bool perdonadoCarcel = false;

        public bool tengoPropsAzules = false;
        public bool tengoPropsVerdes = false;
        public bool tengoPropsRosas = false;
        public bool tengoPropsNaranjas = false;
        public bool tengoPropsCelestes = false;
        public bool tengoPropsAmarillas = false;
        public bool tengoPropsRojas= false;
        public bool tengoPropsBordo = false;

        public Jugador(Propiedad.owner pOwner, int pDinero, string pNombre)
        {
            enumOwnerJugador = pOwner;
            nombre = pNombre;
            dinero = pDinero;
        }
        #region nose si esto va, me parece que no porque esto ya lo hacen las propiedades directamente creo con el gameManager
        public void ComprarCasillero(Propiedad propiedad)
        {
            if(propiedad.comprable)
            {
                propiedad.ownerEnum = enumOwnerJugador;
                propiedad.comprable = false;
            }           
        }

        public void ContruirCasaJugador(Propiedad propiedad)
        {
            propiedad.ContruirCasa(enumOwnerJugador,this);
        }

        public void ContruirHotelJugador(Propiedad propiedad)
        {
            propiedad.ContruirHotel(enumOwnerJugador,this);
        }
        #endregion
        public int TirarDado1()
        {
            int i = Random.Range(1, 7);
            return i;
        }
        public int TirarDado2()
        {
            int i = Random.Range(1, 7);
            return i;
        }
    }
}
