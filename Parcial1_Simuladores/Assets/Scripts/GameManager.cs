using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Monopoly;
using UnityEngine.UI;
using TMPro;
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Tablero tablero = new Tablero();

    public Jugador jugador1 = new Jugador(Propiedad.owner.jugador1,500,"Jugador 1");
    public Jugador jugador2 = new Jugador(Propiedad.owner.jugador2, 500, "Jugador 2");
    public Jugador jugador3 = new Jugador(Propiedad.owner.jugador3, 500, "Jugador 3");
    public Jugador jugador4 = new Jugador(Propiedad.owner.jugador4, 500, "Jugador 4");
    [SerializeField]
    public List<Jugador> jugadores = new List<Jugador>();

    public int turno = 0;
    public bool puedeTirarDados = false;

    public GameObject CartelInicioTurno;
    public Text txtTurno;

    public GameObject CartelAcciones;
    public Text txtAcciones;
    public GameObject bt1;
    public GameObject bt2;
    public GameObject bt3;

    public delegate void  DelegateBotones(Propiedad.owner comprador, Jugador jugador);
    public delegate void DelegateNormal();
    public DelegateBotones delegateBt1;
    public DelegateBotones delegateBt2;
    public DelegateNormal delegateBt3;

    public Transform posCasillerosAdquiridos;
    public MyCasilleros prefMyCasilleros;

    public GameObject CartelDados;
    public Text txtDado1;
    public Text txtDado2;
    public Text txtTotalDados;

    public List<MyCasilleros> listPropsAdquiridas = new List<MyCasilleros>();

    public GameObject goJugador1;
    public GameObject goJugador2;
    public GameObject goJugador3;
    public GameObject goJugador4;
    List<GameObject> goJugadores = new List<GameObject>();
    public List<GameObject> posCasilleros = new List<GameObject>();
    public bool tiraDevuelta = false;
    public int controlTiroDoble = 0;

    public GameObject btBancaRota;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        jugadores.Add(jugador1);
        jugadores.Add(jugador2);
        jugadores.Add(jugador3);
        jugadores.Add(jugador4);
        goJugadores.Add(goJugador1);
        goJugadores.Add(goJugador2);
        goJugadores.Add(goJugador3);
        goJugadores.Add(goJugador4);
        tablero.CrearCasilleros();
        tablero.LlenarCartasArcaComunal();
        tablero.LlenarCartasSuerte();
        OnTurnBegins();
    }

    private void Update()
    {
        if(puedeTirarDados && Input.GetKeyDown(KeyCode.Space))
        {
            CartelInicioTurno.SetActive(false);
            int dado1 = jugadores[turno].TirarDado1();
            int dado2 = jugadores[turno].TirarDado2();
            if(dado1 == dado2)
            {
                tiraDevuelta = true;
            }
            StartCoroutine(Esperar2Secs(dado1, dado2));
            puedeTirarDados = false;
        }
        if(jugadores.Count == 1)
        {
            btBancaRota.SetActive(false);
            CartelInicioTurno.SetActive(false);
            txtAcciones.text = "El ganador es el " + jugadores[0].nombre + ", felicitaciones.";
            bt1.SetActive(false);
            bt2.SetActive(false);
            bt3.SetActive(true);
            bt3.GetComponentInChildren<Text>().text = "Salir del juego";
            delegateBt3 = CloseGame;
            CartelAcciones.SetActive(true);
        }
    }

    public void OnTurnBegins()
    {
        if(jugadores[turno].bancaRota)
        {
            EndTurn();
            return;
        }
        CartelAcciones.SetActive(false);
        if (jugadores[turno].penalizacionTurnos == 0)
        {
            txtTurno.text = "Turno del " + jugadores[turno].nombre;
            CartelInicioTurno.SetActive(true);
            puedeTirarDados = true;
            LlenarCasillerosAdquiridos();
        }
        else
        {
            jugadores[turno].penalizacionTurnos--;
            if(jugadores[turno].penalizacionTurnos != 0 && jugadores[turno].posicion == 10)
            {
                txtAcciones.text = "�Deseas pagar $250 para salir de prision?";
                delegateBt1 = PagarPrision;
                delegateBt3 = EndTurn;
                bt1.SetActive(true);
                bt2.SetActive(false);
                bt3.SetActive(true);
                bt1.GetComponentInChildren<Text>().text = "Pagar";
                bt3.GetComponentInChildren<Text>().text = "Terminar turno";
                CartelAcciones.SetActive(true);
            }
            else
            {
                EndTurn();
            }
        }
    }
    
    public void LlenarCasillerosAdquiridos()
    {
        foreach (Propiedad prop in jugadores[turno].misCasilleros)
        {
            MyCasilleros myCasillero = Instantiate(prefMyCasilleros, posCasillerosAdquiridos);
            myCasillero.SetearTxt(prop);
            listPropsAdquiridas.Add(myCasillero);
        }
    }
    public void EndTurn()
    {
        if(!tiraDevuelta)
        {
            turno++;
            controlTiroDoble = 0;
        }
        if(tiraDevuelta)
        {
            if(controlTiroDoble == 1)
            {
                jugadores[turno].penalizacionTurnos = 3;
                jugadores[turno].posicion = 10;
                MoverJugadoresEnTablero();
                controlTiroDoble = 0;
                turno++;
            }
            else
            {
                controlTiroDoble++;
            }
        }
        if (turno == jugadores.Count)
        {
            turno = 0;
        }
        tiraDevuelta = false;
        DestruirPropsAdq();
        listPropsAdquiridas.Clear();
        OnTurnBegins();
    }

    public void DestruirPropsAdq()
    {
        for (int i = 0; i < listPropsAdquiridas.Count; i++)
        {
            Destroy(listPropsAdquiridas[i].gameObject);
        }

        listPropsAdquiridas.Clear();
    }
    public void JugoJugador(int numero)
    {
        if (jugadores[turno].posicion + numero > tablero.Casilleros.Count)
        {
            jugadores[turno].dinero += 200;
            jugadores[turno].posicion = (jugadores[turno].posicion + numero) - tablero.Casilleros.Count;
        }
        else
        {
            jugadores[turno].posicion += numero;
        }
        MoverJugadoresEnTablero();
        tablero.Casilleros[jugadores[turno].posicion].JugadorOnCasillero(jugadores[turno]);
    }

    public void MoverJugadoresEnTablero()
    {
        if (jugadores[turno].posicion < 10)
        {
            goJugadores[turno].transform.position = new Vector3(posCasilleros[jugadores[turno].posicion].transform.position.x + (1.2f*turno), posCasilleros[jugadores[turno].posicion].transform.position.y, posCasilleros[jugadores[turno].posicion].transform.position.z);        
        }
        if (jugadores[turno].posicion > 9 && jugadores[turno].posicion < 20)
        {
            goJugadores[turno].transform.position = new Vector3(posCasilleros[jugadores[turno].posicion].transform.position.x, posCasilleros[jugadores[turno].posicion].transform.position.y, posCasilleros[jugadores[turno].posicion].transform.position.z - (1.2f * turno));
        }
        if (jugadores[turno].posicion < 30 && jugadores[turno].posicion > 19)
        {
            goJugadores[turno].transform.position = new Vector3(posCasilleros[jugadores[turno].posicion].transform.position.x - (1.2f * turno), posCasilleros[jugadores[turno].posicion].transform.position.y, posCasilleros[jugadores[turno].posicion].transform.position.z);
        }
        if (jugadores[turno].posicion > 29 && jugadores[turno].posicion < 40)
        {
            goJugadores[turno].transform.position = new Vector3(posCasilleros[jugadores[turno].posicion].transform.position.x, posCasilleros[jugadores[turno].posicion].transform.position.y, posCasilleros[jugadores[turno].posicion].transform.position.z + (1.2f * turno));
        }
    }
    public void Boton1()
    {
        delegateBt1(jugadores[turno].enumOwnerJugador, jugadores[turno]);
    }
    public void Boton2()
    {
        delegateBt2(jugadores[turno].enumOwnerJugador, jugadores[turno]);
    }
    public void Boton3()
    {
        delegateBt3();
    }

    IEnumerator Esperar2Secs(int dado1,int dado2)
    {
        txtDado1.text = "Dado 1 = " + dado1.ToString();
        txtDado2.text = "Dado 2 = " + dado2.ToString();
        txtTotalDados.text = "Total = " + (dado1 + dado2).ToString();
        CartelDados.SetActive(true);
        yield return new WaitForSeconds(2);
        CartelDados.SetActive(false);
        JugoJugador(dado2 + dado1);
    }

    public void BancaRota()
    {
        jugadores[turno].bancaRota = true;
        foreach(Propiedad prop in jugadores[turno].misCasilleros)
        {
            prop.ownerEnum = Propiedad.owner.nulo;
        }
        jugadores[turno].misCasilleros.Clear();
        jugadores[turno].dinero = 0;
        jugadores[turno].bancaRota = true;
        jugadores[turno].penalizacionTurnos = int.MaxValue;
        jugadores.Remove(jugadores[turno]);
        goJugadores[turno].SetActive(false);
        goJugadores.Remove(goJugadores[turno]);
        CanvasController.instance.txt[turno].text = "BANCA ROTA";
        CanvasController.instance.txt[turno].color = Color.black;
        CanvasController.instance.txt.Remove(CanvasController.instance.txt[turno]);    
        turno -= 1;
        EndTurn();
    }

    public void PagarPrision(Propiedad.owner comprador, Jugador jugador)
    {
        jugadores[turno].dinero -= 250;
        txtTurno.text = "Turno del " + jugadores[turno].nombre;
        CartelInicioTurno.SetActive(true);
        puedeTirarDados = true;
        LlenarCasillerosAdquiridos();
        jugadores[turno].penalizacionTurnos = 0;
        CartelAcciones.SetActive(false);
    }

    public void CloseGame()
    {
        Application.Quit();
    }
}
