using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Monopoly
{
    public class Carta
    {
        public string queHace;
        public virtual void JugadorAgarroCarta(Propiedad.owner comprador, Jugador jugador) { }
        public void SetearCartel()
        {
            GameManager.instance.delegateBt3 = GameManager.instance.EndTurn;
            GameManager.instance.txtAcciones.text = "Carta: " + queHace;
            GameManager.instance.bt2.SetActive(false);
            GameManager.instance.bt1.SetActive(false);
            GameManager.instance.bt3.SetActive(true);
            GameManager.instance.bt3.GetComponentInChildren<Text>().text = "Terminar turno";
            GameManager.instance.CartelAcciones.SetActive(true);
        }
        public void NoTieneDinero(Jugador jugador)
        {
            GameManager.instance.delegateBt2 = JugadorAgarroCarta;
            GameManager.instance.txtAcciones.text = "(NO TIENES DINERO SUFICIENTE) Carta: " + queHace;
            GameManager.instance.bt2.SetActive(true);
            GameManager.instance.bt1.SetActive(false);
            GameManager.instance.bt3.SetActive(false);
            GameManager.instance.bt2.GetComponentInChildren<Text>().text = "Pagar";
            GameManager.instance.CartelAcciones.SetActive(true);
        }
    }
}
