using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CanvasController : MonoBehaviour
{
    public Text txtDineroJ1;
    public Text txtDineroJ2;
    public Text txtDineroJ3;
    public Text txtDineroJ4;

    public List<Text> txt = new List<Text>();

    public static CanvasController instance;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        txt.Add(txtDineroJ1);
        txt.Add(txtDineroJ2);
        txt.Add(txtDineroJ3);
        txt.Add(txtDineroJ4);
        InvokeRepeating("ActualizarTextos", 0, 1);
    }

    public void ActualizarTextos()
    {
        for (int i = 0; i < GameManager.instance.jugadores.Count; i++)
        {
            txt[i].text = "Dinero: " + GameManager.instance.jugadores[i].dinero.ToString();
        }
    }
}
