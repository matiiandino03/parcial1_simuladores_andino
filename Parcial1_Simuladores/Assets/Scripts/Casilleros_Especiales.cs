using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Monopoly
{
    public class Casilleros_Especiales : Casillero
    {
        public int impuestoValor;
        public Casilleros_Especiales(Casillero.type mType)
        {
            typeEnum = mType;
        }
        public Casilleros_Especiales(Casillero.type mType, int valorImpuesto)
        {
            typeEnum = mType;
            impuestoValor = valorImpuesto;
        }
        public override void JugadorOnCasillero(Jugador jugador)
        {
            if(typeEnum == Casillero.type.salida)
            {
                jugador.dinero += 200;
                GameManager.instance.bt1.SetActive(false);
                GameManager.instance.bt2.SetActive(false);
                GameManager.instance.bt3.SetActive(true);
                GameManager.instance.delegateBt3 = GameManager.instance.EndTurn;
                GameManager.instance.bt3.GetComponentInChildren<Text>().text = "Terminar turno";
                GameManager.instance.txtAcciones.text = "Pasaste por la salida, recibes $200";
                GameManager.instance.CartelAcciones.SetActive(true);
            }
            if(typeEnum == Casillero.type.carcel)
            {
                GameManager.instance.bt1.SetActive(false);
                GameManager.instance.bt2.SetActive(false);
                GameManager.instance.bt3.SetActive(true);
                GameManager.instance.delegateBt3 = GameManager.instance.EndTurn;
                GameManager.instance.bt3.GetComponentInChildren<Text>().text = "Terminar turno";
                GameManager.instance.txtAcciones.text = "Estas de visita en la carcel";
                GameManager.instance.CartelAcciones.SetActive(true);
            }
            if (typeEnum == Casillero.type.suerte)
            {
                int random = Random.Range(0, GameManager.instance.tablero.CartasSuerte.Count);
                GameManager.instance.tablero.CartasSuerte[random].JugadorAgarroCarta(jugador.enumOwnerJugador, jugador);
            }
            if (typeEnum == Casillero.type.cofre)
            {
                int random = Random.Range(0, GameManager.instance.tablero.CartasArcaComunal.Count);
                GameManager.instance.tablero.CartasArcaComunal[random].JugadorAgarroCarta(jugador.enumOwnerJugador,jugador);
            }
            if (typeEnum == Casillero.type.impuesto)
            {
                if(jugador.dinero >= impuestoValor)
                {
                    GameManager.instance.bt1.SetActive(false);
                    GameManager.instance.bt2.SetActive(true);
                    GameManager.instance.bt3.SetActive(false);
                    GameManager.instance.delegateBt2 = PagarImpuesto;
                    GameManager.instance.bt2.GetComponentInChildren<Text>().text = "Pagar Impuesto";
                    GameManager.instance.txtAcciones.text = "Debes pagar un impuesto de $" + impuestoValor.ToString() + " al banco";
                    GameManager.instance.CartelAcciones.SetActive(true);
                }
                else
                {
                    GameManager.instance.txtAcciones.text = "Debes pagar un impuesto de $" + impuestoValor.ToString() + " al banco, si no te alcanza el dinero declarate en banca rota";
                }
            }
            if (typeEnum == Casillero.type.goToJail)
            {
                if(!jugador.perdonadoCarcel)
                {
                    jugador.penalizacionTurnos = 3;
                    jugador.posicion = 10;
                }
                if(jugador.perdonadoCarcel)
                {
                    GameManager.instance.bt1.SetActive(false);
                    GameManager.instance.bt2.SetActive(false);
                    GameManager.instance.bt3.SetActive(true);
                    GameManager.instance.delegateBt3 = GameManager.instance.EndTurn;
                    GameManager.instance.bt3.GetComponentInChildren<Text>().text = "Terminar turno";
                    GameManager.instance.txtAcciones.text = "Te salvas de ir a la carcel debido a tu carta de suerte";
                    GameManager.instance.CartelAcciones.SetActive(true);
                    jugador.perdonadoCarcel = false;
                }
            }
            if (typeEnum == Casillero.type.free)
            {
                GameManager.instance.bt1.SetActive(false);
                GameManager.instance.bt2.SetActive(false);
                GameManager.instance.bt3.SetActive(true);
                GameManager.instance.delegateBt3 = GameManager.instance.EndTurn;
                GameManager.instance.bt3.GetComponentInChildren<Text>().text = "Terminar turno";
                GameManager.instance.txtAcciones.text = "Casillero libre, no pasa nada";
                GameManager.instance.CartelAcciones.SetActive(true);
            }
        }
        public void PagarImpuesto(Propiedad.owner comprador, Jugador jugador)
        {
            jugador.dinero -= impuestoValor;
            GameManager.instance.EndTurn();
        }
    }
}
