using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject camera_1;
    public GameObject camera_2;
    public GameObject camera_3;
    public GameObject camera_4;

    int camera_number = 0;
    void Start()
    {
        camera_1.SetActive(true);
        camera_2.SetActive(false);
        camera_3.SetActive(false);
        camera_4.SetActive(false);
    }


    void Update()
    {
        if(camera_number > 3)
        {
            camera_number = 0;
            ChangeCamera(camera_number);
        }
        if(Input.GetKeyDown(KeyCode.A))
        {
            camera_number++;
            ChangeCamera(camera_number);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            if(camera_number == 0)
            {
                camera_number = 3;
            }
            else
            {
                camera_number--;
            }
            ChangeCamera(camera_number);
        }
    }

    void ChangeCamera(int camera)
    {
        if(camera == 0)
        {
            camera_1.SetActive(true);
            camera_2.SetActive(false);
            camera_3.SetActive(false);
            camera_4.SetActive(false);
        }
        else if (camera == 1)
        {
            camera_1.SetActive(false);
            camera_2.SetActive(true);
            camera_3.SetActive(false);
            camera_4.SetActive(false);
        }
        else if (camera == 2)
        {
            camera_1.SetActive(false);
            camera_2.SetActive(false);
            camera_3.SetActive(true);
            camera_4.SetActive(false);
        }
        else if (camera == 3)
        {
            camera_1.SetActive(false);
            camera_2.SetActive(false);
            camera_3.SetActive(false);
            camera_4.SetActive(true);
        }
    }
}
